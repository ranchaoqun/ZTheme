package com.rxx.ztheme

import org.gradle.api.Plugin
import org.gradle.api.Project
import com.android.build.gradle.AppPlugin
import com.android.build.gradle.AppExtension
/**
 * @author 冉超群
 * @date 2017/12/5-17:35
 * @desc https://www.2cto.com/kf/201604/499386.html
 */
public class ThemePlug implements Plugin<Project> {
    void apply(Project project) {
        System.out.println("========================")
        System.out.println("start do ThemePlug")
        System.out.println("========================")
        if (project.plugins.hasPlugin(AppPlugin)) {
            System.out.println("add ClassInjectionTransform")
            def android = project.extensions.getByType(AppExtension)
            def transform = new ClassInjectionTransform(project)
            android.registerTransform(transform)
        } else {
            System.out.println("ThemePlug has add")
        }
    }
}
