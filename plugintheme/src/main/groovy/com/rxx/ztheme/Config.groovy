package com.rxx.ztheme
/**
 * @author 冉超群
 * @date 2017/12/5-17:35
 * @desc
 */
public interface Config {

    String INJECT_JAR_PATH = "/src/main/assets/theme/zthemeinject.jar"

    String INJECT_CONTENT = "if(Boolean.FALSE.booleanValue()){com.rxx.theme.core.ZThemeInject.doEmpty();}"

    String TASK_NAME = "ClassInjectionTransform"

    String INJECT_RULE_FILE_NAME = "ztheme-inject.config"

    String KEY_ANDROID_JAR = "android-jar"

    String KEY_NO_INJECT_CLASS = "no-inject"
}