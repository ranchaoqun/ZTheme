package com.rxx.theme;

import java.io.File;

/**
 * @author 冉超群
 * @date 2017/11/14-14:15
 * @desc
 */
public class ZThemeFileEntity {
    /**
     * 主题包所在路径
     */
    public String themePath;

    /**
     * 主题包apk文件名称
     */
    public String apkName;

    /**
     * 主题包dex名称
     */
    public String dexName;

    /**
     * so名称
     */
    public String soName;

    /**
     * 主题包名称，取自app
     */
    public String themeName;

    public ZThemeFileEntity(String themePath, String apkName, String dexName, String soName, String themeName) {
        this.themePath = themePath;
        this.apkName = apkName;
        this.dexName = dexName;
        this.soName = soName;
        this.themeName = themeName;
    }

    public String getApkPath() {
        return themePath + File.separator + apkName;
    }

    public String getDexPath() {
        return themePath + File.separator + dexName;
    }

    public String getSoPath() {
        return themePath + File.separator + soName;
    }
}
