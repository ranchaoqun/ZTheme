package com.rxx.theme;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;

import com.rxx.theme.context.ZContextThemeWrapper;
import com.rxx.theme.context.ZContextWrapper;
import com.rxx.theme.utils.ZThemeResourceUtils;
import com.rxx.theme.view.IThemeView;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import dalvik.system.DexClassLoader;
import dalvik.system.PathClassLoader;

/**
 * @author 冉超群
 * @date 2017/10/19-17:39
 * @desc
 */
public class ZTheme {

    private final static String TAG = "ZTheme";

    /**
     * 存放 theme，key:主题包路径 value:view
     */
    private static Map<String, ZTheme> cacheThemes = new HashMap<>();

    /**
     * ZThemeLayoutInflaterFactory 加载自定义View关键字
     */
    private static HashSet<String> loadViewClassKey = new HashSet<>();

    /**
     * 主题包原始Context
     */
    private Context baseContext;

    /**
     * ZThemeFileEntity
     */
    private ZThemeFileEntity themeFileEntity;

    /**
     * ClassLoader
     */
    private ClassLoader themeClassLoader;

    /**
     * key:view 对象的className
     * value：ZThemeView
     */
    private Map<String, IThemeView> themeViews = new HashMap<>();

    /**
     * 自定义加载ThemeView
     */
    private Factory factory;

    public ZTheme(Context baseContext, ZThemeFileEntity themeFileEntity, ClassLoader themeClassLoader) {
        this.baseContext = baseContext;
        this.themeFileEntity = themeFileEntity;
        this.themeClassLoader = themeClassLoader;
    }

    /**
     * 指定主题是否缓存
     *
     * @param path 主题包路径
     * @return 指定主题是否缓存
     */
    public static boolean themeIsCached(String path) {
        return cacheThemes.containsKey(path);
    }

    /**
     * @param themeFileEntity
     * @param context
     * @return
     */
    public static ZTheme createTheme(ZThemeFileEntity themeFileEntity, Context context) {
        if (themeFileEntity == null) {
            throw new RuntimeException("themeFileEntity is null");
        }

        if (TextUtils.isEmpty(themeFileEntity.themePath)) {
            throw new RuntimeException("themeFileEntity is null");
        }

        if (TextUtils.isEmpty(themeFileEntity.dexName)) {
            throw new RuntimeException("dexName is null");
        }

        if (TextUtils.isEmpty(themeFileEntity.apkName)) {
            throw new RuntimeException("apkName is null");
        }

        ZTheme tITheme = null;
        if (cacheThemes.containsKey(themeFileEntity.getApkPath())) {
            tITheme = cacheThemes.get(themeFileEntity.getApkPath());
        } else {
            long startDexClassLoaderTime = System.currentTimeMillis();
            PackageInfo packageInfo = ZThemeResourceUtils.loadApk(context, themeFileEntity.getApkPath());
            Log.d(TAG, "loadApkTime" + (System.currentTimeMillis() - startDexClassLoaderTime));

            ClassLoader tClassLoader = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                tClassLoader = new PathClassLoader(themeFileEntity.getDexPath(),
                        themeFileEntity.getSoPath(),
                        context.getClassLoader());
            } else {
                tClassLoader = new DexClassLoader(themeFileEntity.getApkPath(),
                        themeFileEntity.themePath,
                        themeFileEntity.getSoPath(),
                        context.getClassLoader());
            }
            Log.d(TAG, "DexClassLoaderTime" + (System.currentTimeMillis() - startDexClassLoaderTime));
            if (context instanceof Activity) {
                Context baseContext = createActivityContext((Activity) context);

                Resources tResources = ZThemeResourceUtils.createResources(baseContext, themeFileEntity.getApkPath());
                ZThemeResourceUtils.replaceContextImplResources(baseContext, tResources);
                ZThemeResourceUtils.replaceContextImplTheme(baseContext, packageInfo.applicationInfo.theme);

                ZContextThemeWrapper tZContextThemeWrapper = new ZContextThemeWrapper(baseContext, packageInfo.applicationInfo.theme, tClassLoader);
                ZThemeResourceUtils.replaceContextThemeWrapperResources(tZContextThemeWrapper, tResources);
                ZThemeResourceUtils.replaceContextThemeWrapper(tZContextThemeWrapper, packageInfo.applicationInfo.theme);

                tITheme = new ZTheme(tZContextThemeWrapper, themeFileEntity, tClassLoader);
            } else if (context instanceof Application) {
                Context baseContext = createAppContext((Application) context);

                Resources tResources = ZThemeResourceUtils.createResources(baseContext, themeFileEntity.getApkPath());
                ZThemeResourceUtils.replaceContextImplResources(baseContext, tResources);

                ZThemeResourceUtils.replaceContextImplTheme(baseContext, packageInfo.applicationInfo.theme);

                tITheme = new ZTheme(new ZContextWrapper(baseContext, tClassLoader), themeFileEntity, tClassLoader);
            }
            if (tITheme != null) {
                cacheThemes.put(themeFileEntity.getApkPath(), tITheme);
            }
            Log.d(TAG, "LoaderEndTime" + (System.currentTimeMillis() - startDexClassLoaderTime));
        }
        return tITheme;
    }

    /**
     * 配置自定义View加载关键字
     *
     * @param loadViewClassKeys
     */
    public static void addViewClassKey(String[] loadViewClassKeys) {
        loadViewClassKey.addAll(Arrays.asList(loadViewClassKeys));
    }

    public static HashSet<String> getLoadViewClassKey() {
        return loadViewClassKey;
    }

    private static Context createAppContext(Application application) {
        Context context = null;
        Object contextImpl = application.getBaseContext();
        try {
            Class tContextImplClass = Class.forName("android.app.ContextImpl");

            Field tLoadedApkField = tContextImplClass.getDeclaredField("mPackageInfo");
            tLoadedApkField.setAccessible(true);
            Object tLoadedApk = tLoadedApkField.get(contextImpl);

            Field tActivityThreadField = tContextImplClass.getDeclaredField("mMainThread");
            tActivityThreadField.setAccessible(true);
            Object tActivityThread = tActivityThreadField.get(contextImpl);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                Method tCreateAppContextMethod = tContextImplClass.getDeclaredMethod("createAppContext", tActivityThread.getClass(), tLoadedApk.getClass());
                tCreateAppContextMethod.setAccessible(true);
                context = (Context) tCreateAppContextMethod.invoke(contextImpl, tActivityThread, tLoadedApk);
            } else {
                Constructor tConstructor = tContextImplClass.getDeclaredConstructor();
                tConstructor.setAccessible(true);
                context = (Context) tConstructor.newInstance();
                Method tInitMethod = tContextImplClass.getDeclaredMethod("init", tLoadedApk.getClass(), IBinder.class, tActivityThread.getClass());
                tInitMethod.setAccessible(true);
                tInitMethod.invoke(context, tLoadedApk, null, tActivityThread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context;
    }

    private static Context createActivityContext(Activity activity) {
        Context context = null;
        Object contextImpl = activity.getBaseContext();
        try {
            Class tContextImplClass = Class.forName("android.app.ContextImpl");

            Field tLoadedApkField = tContextImplClass.getDeclaredField("mPackageInfo");
            tLoadedApkField.setAccessible(true);
            Object tLoadedApk = tLoadedApkField.get(contextImpl);

            Field tActivityThreadField = tContextImplClass.getDeclaredField("mMainThread");
            tActivityThreadField.setAccessible(true);
            Object tActivityThread = tActivityThreadField.get(contextImpl);

            Field tActivityTokenField = tContextImplClass.getDeclaredField("mActivityToken");
            tActivityTokenField.setAccessible(true);
            Object tActivityToken = tActivityTokenField.get(contextImpl);

            Field tDisplayField = tContextImplClass.getDeclaredField("mDisplay");
            tDisplayField.setAccessible(true);
            Display tDisplay = (Display) tDisplayField.get(contextImpl);

            Class tActivityClass = Class.forName("android.app.Activity");
            Field tConfigurationField = tActivityClass.getDeclaredField("mCurrentConfig");
            tConfigurationField.setAccessible(true);
            Object tConfiguration = tConfigurationField.get(activity);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH) {
                //~19
                Constructor tConstructor = tContextImplClass.getDeclaredConstructor();
                tConstructor.setAccessible(true);
                context = (Context) tConstructor.newInstance();
                Method tInitMethod = tContextImplClass.getDeclaredMethod("init", tLoadedApk.getClass(), IBinder.class, tActivityThread.getClass());
                tInitMethod.setAccessible(true);
                tInitMethod.invoke(context, tLoadedApk, tActivityTokenField, tActivityThread);
            } else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                //20,21,22
                Method tMethod=tContextImplClass.getDeclaredMethod("createActivityContext",tActivityThread.getClass(),tLoadedApk.getClass(),IBinder.class);
                tMethod.setAccessible(true);
                context = (Context) tMethod.invoke(contextImpl, tActivityThread, tLoadedApk, tActivityToken);
            }else{
                //23~
                Method tMethod=tContextImplClass.getDeclaredMethod("createActivityContext",tActivityThread.getClass(),
                        tLoadedApk.getClass(),
                        IBinder.class,
                        tDisplay.getClass(),
                        tConfiguration.getClass());
                tMethod.setAccessible(true);
                context = (Context) tMethod.invoke(contextImpl, tActivityThread, tLoadedApk, tActivityToken, tDisplay.getDisplayId(), tConfiguration);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return context;
    }

    public boolean checkSecure() {
        return true;
    }

    /**
     * Default
     *
     * @param className
     * @return
     */
    public IThemeView getThemeView(String className) {
        return getThemeView(className, false);
    }

    /**
     * 是否使用缓存
     *
     * @param className view 类名
     * @param cached    是否使用缓存
     * @return
     */
    public IThemeView getThemeView(String className, boolean cached) {
        if (cached && viewIsCached(className)) {
            return themeViews.get(className);
        }
        IThemeView tIThemeView = null;
        if (factory != null) {
            tIThemeView = factory.getThemeView(className);
        }
        if (tIThemeView == null) {
            try {
                Class<?> tClass = getDexClassLoader().loadClass(className);
                Constructor<?> tConstructor = tClass.getConstructor(Context.class, ZTheme.class);
                tConstructor.setAccessible(true);
                tIThemeView = (IThemeView) tConstructor.newInstance(baseContext, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cached && tIThemeView != null && !themeViews.containsKey(className)) {
            themeViews.put(className, tIThemeView);

        }
        return tIThemeView;
    }

    public ClassLoader getDexClassLoader() {
        return themeClassLoader;
    }

    public String getThemePath() {
        return themeFileEntity.themePath;
    }

    public String getSoPath() {
        return themeFileEntity.getSoPath();
    }

    public boolean viewIsCached(String className) {
        return themeViews.containsKey(className);
    }

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    /**
     * 创建View对外暴露接口，可以自己实现
     */
    public interface Factory {
        IThemeView getThemeView(String className);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ZTheme)) {
            return false;
        }

        ZTheme zTheme = (ZTheme) o;

        if (getThemePath() != null ? !getThemePath().equals(zTheme.getThemePath()) : zTheme.getThemePath() != null) {
            return false;
        }
        return themeFileEntity.getSoPath() != null ? themeFileEntity.getSoPath().equals(zTheme.getSoPath()) : zTheme.getSoPath() == null;
    }

}
