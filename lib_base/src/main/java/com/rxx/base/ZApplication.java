package com.rxx.base;

import android.app.Application;
import android.content.Context;

import com.rxx.base.core.ZThemeManager;
import com.rxx.base.theme.ThemeChange;
import com.rxx.theme.ZTheme;
import com.rxx.theme.ZThemeFileEntity;

/**
 * @author 冉超群
 * @date 2017/10/27-10:55
 * @desc
 */
public class ZApplication<VR extends ViewRelated> extends Application {

    public ZTheme getZTheme() {
        return ZThemeManager.getInstance().getZTheme();
    }

    /**
     * add  ThemeChange
     *
     * @param tag
     */
    public void addThemeChange(ThemeChange tag) {
        ZThemeManager.getInstance().addThemeChangeListener(tag);
    }

    /**
     * 移除 ThemeChange
     *
     * @param tag
     */
    public void remove(ThemeChange tag) {
        ZThemeManager.getInstance().removeThemeChangeListener(tag);
    }

    /**
     * 切换主题
     *
     * @param themeFileEntity
     */
    public void changeTheme(ZThemeFileEntity themeFileEntity, Context context) {
        ZThemeManager.getInstance().changeTheme(themeFileEntity, context);
    }

    public VR getViewRelated() {
        return (VR) ZThemeManager.getInstance().getViewRelated();
    }
}
