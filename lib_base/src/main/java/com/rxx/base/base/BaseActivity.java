package com.rxx.base.base;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.rxx.base.R;
import com.rxx.base.ZApplication;
import com.rxx.base.core.IController;
import com.rxx.base.theme.ThemeChange;
import com.rxx.theme.ZTheme;

/**
 * @author 冉超群
 * @date 2017/10/26-20:32
 * @desc BaseController
 */
public class BaseActivity<IV extends BaseView<IC>, IC extends BaseActivity> extends AppCompatActivity implements IController<IV>, ThemeChange {

    protected IV mView;

    protected FrameLayout mZthemeRoot;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base);
        mZthemeRoot= (FrameLayout) findViewById(R.id.mZthemeRoot);
        zGetApplication().addThemeChange(this);
        doView();
        zOnCreate(savedInstanceState);
    }

    @Override
    public void setContentView(View view) {
        mZthemeRoot.removeAllViews();
        mZthemeRoot.addView(view);
    }

    @Override
    public void setContentView(int layoutResID) {
        mZthemeRoot.removeAllViews();
        mZthemeRoot.addView(View.inflate(this, layoutResID,mZthemeRoot));
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        mZthemeRoot.removeAllViews();
        mZthemeRoot.addView(view,params);
    }

    /**
     * 替代 onCreate
     *
     * @param savedInstanceState
     */
    protected void zOnCreate(@Nullable Bundle savedInstanceState) {

    }

    @Override
    public void setView(IV view) {
        mView = view;
    }

    @Override
    public String getViewTag() {
        return "";
    }

    @Override
    public final void onThemeChange(ZTheme zTheme) {
        doView();
        zOnThemeChange(zTheme);
    }

    private void doView() {
        setView(createThemeView(zGetApplication().getViewRelated().getClassNameByTag(getViewTag())));
        mView.setController((IC) this);
        setContentView(mView.getView());
        setWindowStatusBarColor(mView.getWindowStatusBarColor());
        setWindowNavigationBarColor(mView.getWindowNavigationBarColor());
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        System.out.println("onContentChanged");
    }

    /**
     * 替代 zOnThemeChange
     *
     * @param zTheme
     */
    public void zOnThemeChange(ZTheme zTheme) {

    }

    /**
     * 获取ThemeView
     *
     * @param
     */
    private final IV createThemeView(String className) {
        return (IV) zGetApplication().getZTheme().getThemeView(className);
    }

    /**
     * 获取Application
     *
     * @return
     */
    public ZApplication zGetApplication() {
        return (ZApplication) getApplication();
    }


    @Override
    protected final void onDestroy() {
        super.onDestroy();
        zGetApplication().remove(this);
        mView.deleteController();
        zOnDestroy();
    }

    /**
     * 提到onDestroy
     */
    protected void zOnDestroy() {

    }

    /**
     * 设置状态栏颜色
     *
     * @param color
     */
    public void setWindowStatusBarColor(int color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 切换底部导航栏颜色
     *
     * @param color
     */
    public void setWindowNavigationBarColor(int color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setNavigationBarColor(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
