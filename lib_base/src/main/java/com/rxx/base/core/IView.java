package com.rxx.base.core;

import android.view.View;

/**
 * @author 冉超群
 * @date 2017/10/26-20:27
 * @desc
 */
public interface IView<IC extends IController> {

    /**
     * 设置Controller
     * @param controller
     */
    void setController(IC controller);

    /**
     * 删除Controller
     */
    void deleteController();

    /**
     * 获取 View
     *
     * @return
     */
    View getView();

    /**
     * 设置状状态栏颜色
     *
     * @return
     */
    int getWindowStatusBarColor();

    int getWindowNavigationBarColor();
}
