package com.rxx.ztheme.dark.activity;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;

import com.rxx.ztheme.dark.view.LoginView;

/**
 * @author 冉超群
 * @date 2017/10/29-15:55
 * @desc
 */
public class TestActivity extends Activity {

    LoginView view;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new LoginView(this, null);
        setContentView(view.getView());
        setWindowNavigationBarColor(view.getWindowStatusBarColor());
        setWindowStatusBarColor(view.getWindowNavigationBarColor());

        view.getView().postDelayed(new Runnable() {
            @Override
            public void run() {
                setWindowStatusBarColor(view.getWindowStatusBarColor());
                setWindowNavigationBarColor(view.getWindowNavigationBarColor());
            }
        }, 10000);
    }

    /**
     * 设置状态栏颜色
     *
     * @param color
     */
    public void setWindowStatusBarColor(int color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 切换底部导航栏颜色
     *
     * @param color
     */
    public void setWindowNavigationBarColor(int color) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setNavigationBarColor(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
